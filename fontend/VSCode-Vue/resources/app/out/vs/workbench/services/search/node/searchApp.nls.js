/*!--------------------------------------------------------
 * Copyright (C) Microsoft Corporation. All rights reserved.
 *--------------------------------------------------------*/
define("vs/workbench/services/search/node/searchApp.nls",{"vs/base/common/errorMessage":["{0}: {1}","A system error occurred ({0})","An unknown error occurred. Please consult the log for more details.","An unknown error occurred. Please consult the log for more details.","{0} ({1} errors in total)","An unknown error occurred. Please consult the log for more details."],"vs/base/node/processes":["Can't execute a shell command on a UNC drive."]});
//# sourceMappingURL=https://ticino.blob.core.windows.net/sourcemaps/33c79d5ad447956814a2a3658029dffb9e28bae6/core/vs/workbench/services/search/node/searchApp.nls.js.map
