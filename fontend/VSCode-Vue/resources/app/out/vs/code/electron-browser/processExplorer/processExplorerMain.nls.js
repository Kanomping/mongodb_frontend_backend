/*!--------------------------------------------------------
 * Copyright (C) Microsoft Corporation. All rights reserved.
 *--------------------------------------------------------*/
define("vs/code/electron-browser/processExplorer/processExplorerMain.nls",{"vs/code/electron-browser/processExplorer/processExplorerMain":["CPU %","Memory (MB)","pid","Name","Kill Process","Force Kill Process","Copy","Copy All","Debug"]});
//# sourceMappingURL=https://ticino.blob.core.windows.net/sourcemaps/33c79d5ad447956814a2a3658029dffb9e28bae6/core/vs/code/electron-browser/processExplorer/processExplorerMain.nls.js.map
