/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
"use strict";const bootstrapWindow=require("../../../../bootstrap-window");bootstrapWindow.load(["vs/code/electron-browser/processExplorer/processExplorerMain"],(function(o,r){o.startup(r.data)}),{forceEnableDeveloperKeybindings:!0});
//# sourceMappingURL=https://ticino.blob.core.windows.net/sourcemaps/33c79d5ad447956814a2a3658029dffb9e28bae6/core/vs/code/electron-browser/processExplorer/processExplorer.js.map
