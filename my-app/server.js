const express = require('express')
const app = express()

app.get('/', (req, res, next) => {
  res.json({ message: 'Hello Suthicha!!' })
})

app.listen(9001, () => {
  console.log('Aplication is running  port 9001')
})
