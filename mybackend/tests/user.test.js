const dbHandler = require('./db-handler')
const User = require('../models/User')

beforeAll(async () => {
  await dbHandler.connect()
})

afterEach(async () => {
  await dbHandler.clearDatabase()
})

afterAll(async () => {
  await dbHandler.closeDatabase()
})

const userComplete1 = {
  name: 'Kob',
  gender: 'M'
}

const userComplete2 = {
  name: 'Kob',
  gender: 'F'
}

const userErrorNameEmpty = {
  name: '',
  gender: 'M'
}

const userErrorName2Alphabets = {
  name: 'Ko',
  gender: 'M'
}

const userErrorGenderInvalid = {
  name: 'Ko',
  gender: 'A'
}

describe('User', () => {
  it('Can add new user', async () => {
    let error = null
    try {
      const user = new User(userComplete1)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('Can add new user', async () => {
    let error = null
    try {
      const user = new User(userComplete2)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('Cannot add user because name is empty', async () => {
    let error = null
    try {
      const user = new User(userErrorNameEmpty)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('Cannot add user because name is 2 character', async () => {
    let error = null
    try {
      const user = new User(userErrorName2Alphabets)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('Cannot add user because gender is incorrect ', async () => {
    let error = null
    try {
      const user = new User(userErrorGenderInvalid)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('Cannot add user because name is the same. ', async () => {
    let error = null
    try {
      const user1 = new User(userComplete1)
      await user1.save()
      const user2 = new User(userComplete1)
      await user2.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
